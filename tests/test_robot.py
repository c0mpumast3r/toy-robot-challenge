import pytest

from util.base_exceptions import InvalidCommandException


class TestCommandParsing:
    """Test Command Parsing"""

    def test_valid_place_cmd(self, robot):
        (cmd, args) = robot.parse('PLACE 1,2,NORTH\n')
        assert cmd == 'PLACE'
        assert args['x'] == 1
        assert args['y'] == 2
        assert args['direction'] == 'NORTH'

    def test_valid_place_cmd_with_spaced_coords(self, robot):
        (cmd, args) = robot.parse('PLACE 2, 2, EAST\n')
        assert cmd == 'PLACE'
        assert args['x'] == 2
        assert args['y'] == 2
        assert args['direction'] == 'EAST'

    def test_valid_place_cmd_small_letters(self, robot):
        (cmd, args) = robot.parse('place 3,1,WEST\n')
        assert cmd == 'PLACE'
        assert args['x'] == 3
        assert args['y'] == 1
        assert args['direction'] == 'WEST'

    def test_valid_move_cmd(self, robot):
        (cmd, args) = robot.parse('MOVE\n')
        assert cmd == 'MOVE'
        assert args is None

    def test_valid_left_cmd(self, robot):
        (cmd, args) = robot.parse('LEFT\n')
        assert cmd == 'LEFT'
        assert args is None

    def test_valid_right_cmd(self, robot):
        (cmd, args) = robot.parse('RIGHT\n')
        assert cmd == 'RIGHT'
        assert args is None

    def test_valid_report_cmd(self, robot):
        (cmd, args) = robot.parse('REPORT\n')
        assert cmd == 'REPORT'
        assert args is None

    def test_invalid_cmd(self, robot):
        with pytest.raises(InvalidCommandException):
            (cmd, args) = robot.parse('BAD ROBOT\n')

    def test_missing_coordinates(self, robot):
        with pytest.raises(InvalidCommandException):
            (cmd, args) = robot.parse('PLACE\n')

    def test_bad_facing_value(self, robot):
        with pytest.raises(InvalidCommandException):
            (cmd, args) = robot.parse('PLACE 0,0,DOWN\n')

    def test_bad_x_value(self, robot):
        with pytest.raises(InvalidCommandException):
            (cmd, args) = robot.parse('PLACE a,0,NORTH\n')

    def test_bad_y_value(self, robot):
        with pytest.raises(InvalidCommandException):
            (cmd, args) = robot.parse('PLACE 0,b,NORTH\n')


class TestMovement:
    def test_valid_traverse_clockwise(self, robot, capsys):
        """Traverse the edges of the board clockwise"""

        robot.place({'x': 0, 'y': 0, 'direction': 'NORTH'})
        robot.move()
        robot.move()
        robot.move()
        robot.move()
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,4,NORTH\n'

        robot.right()
        robot.move()
        robot.move()
        robot.move()
        robot.move()
        robot.report()
        out, err = capsys.readouterr()
        assert out == '4,4,EAST\n'

        robot.right()
        robot.move()
        robot.move()
        robot.move()
        robot.move()
        robot.report()
        out, err = capsys.readouterr()
        assert out == '4,0,SOUTH\n'

        robot.right()
        robot.move()
        robot.move()
        robot.move()
        robot.move()
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,WEST\n'

    def test_valid_traverse_counter_clockwise(self, robot, capsys):
        """Traverse the edges of the board counter-clockwise"""

        robot.place({'x': 0, 'y': 0, 'direction': 'EAST'})
        robot.move()
        robot.move()
        robot.move()
        robot.move()
        robot.report()
        out, err = capsys.readouterr()
        assert out == '4,0,EAST\n'

        robot.left()
        robot.move()
        robot.move()
        robot.move()
        robot.move()
        robot.report()
        out, err = capsys.readouterr()
        assert out == '4,4,NORTH\n'

        robot.left()
        robot.move()
        robot.move()
        robot.move()
        robot.move()
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,4,WEST\n'

        robot.left()
        robot.move()
        robot.move()
        robot.move()
        robot.move()
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,SOUTH\n'
