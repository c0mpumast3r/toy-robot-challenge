class TestPosition:
    """Test Positions"""

    def test_valid_direction(self, robot, capsys):

        robot.place({'x': 0, 'y': 0, 'direction': 'NORTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,NORTH\n'

        robot.place({'x': 0, 'y': 0, 'direction': 'WEST'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,WEST\n'

        robot.place({'x': 0, 'y': 0, 'direction': 'SOUTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,SOUTH\n'

        robot.place({'x': 0, 'y': 0, 'direction': 'EAST'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,EAST\n'

    def test_valid_middle(self, robot, capsys):

        robot.place({'x': 1, 'y': 3, 'direction': 'SOUTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '1,3,SOUTH\n'

    def test_valid_edges(self, robot, capsys):

        robot.place({'x': 0, 'y': 3, 'direction': 'SOUTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,3,SOUTH\n'

        robot.place({'x': 2, 'y': 0, 'direction': 'SOUTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '2,0,SOUTH\n'

        robot.place({'x': 4, 'y': 2, 'direction': 'SOUTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '4,2,SOUTH\n'

        robot.place({'x': 3, 'y': 4, 'direction': 'SOUTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '3,4,SOUTH\n'

    def test_valid_corners(self, robot, capsys):

        robot.place({'x': 0, 'y': 0, 'direction': 'SOUTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,SOUTH\n'

        robot.place({'x': 4, 'y': 0, 'direction': 'SOUTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '4,0,SOUTH\n'

        robot.place({'x': 0, 'y': 4, 'direction': 'SOUTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,4,SOUTH\n'

        robot.place({'x': 4, 'y': 4, 'direction': 'SOUTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '4,4,SOUTH\n'

    def test_invalid_position(self, robot, capsys):

        # Set to known valid position.  All invalid attempts at positioning
        # should silently fail and not change the current position
        robot.place({'x': 0, 'y': 0, 'direction': 'NORTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,NORTH\n'

        # Off the grid
        robot.place({'x': 10, 'y': 10, 'direction': 'NORTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,NORTH\n'
        assert err == ''

        # Bad direction
        robot.place({'x': 10, 'y': 10, 'direction': 'FOOBAR'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,NORTH\n'
        assert err == ''

        # Coords are not integers
        robot.place({'x': 0.1, 'y': 0.1, 'direction': 'NORTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,NORTH\n'
        assert err == ''

        # Almost on the board
        robot.place({'x': 6, 'y': 3, 'direction': 'NORTH'})
        robot.report()
        out, err = capsys.readouterr()
        assert out == '0,0,NORTH\n'
        assert err == ''

    def test_no_position(self, robot, capsys):
        """Can't move the robot if it does not have a position"""
        robot.report()
        out, err = capsys.readouterr()
        assert out == ''
        assert err == ''

        robot.move()
        robot.report()
        out, err = capsys.readouterr()
        assert out == ''
        assert err == ''

        robot.left()
        robot.report()
        out, err = capsys.readouterr()
        assert out == ''
        assert err == ''

        robot.right()
        robot.report()
        out, err = capsys.readouterr()
        assert out == ''
        assert err == ''
