#! ../env/bin/python

import pytest


@pytest.fixture
def robot():
    """A simple fixture providing a robot instance"""
    from robot import Robot
    from util.grid import Grid

    grid = Grid(x_range=5, y_range=5)
    robot = Robot()
    robot.learn(grid=grid)
    return robot
