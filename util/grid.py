class Grid:
    """Class to represent Grid object. Contains all attributes about grid."""

    def __init__(self, x_range: int = 5, y_range: int = 5):
        """Constructs all the necessary attributes for the Grid object

        Args:
            x_range (int, optional): limit for x-axis in grid. Defaults to 5.
            y_range (int, optional): limit for y-axis in grid. Defaults to 5.
        """

        self.x_range = [i for i in range(0, x_range)]
        self.y_range = [i for i in range(0, y_range)]

        self.directions = ['NORTH', 'EAST', 'SOUTH', 'WEST']
        self.compass = {
            'NORTH': {
                'left': 'WEST',
                'right': 'EAST'
            },
            'WEST': {
                'left': 'SOUTH',
                'right': 'NORTH'
            },
            'SOUTH': {
                'left': 'EAST',
                'right': 'WEST'
            },
            'EAST': {
                'left': 'NORTH',
                'right': 'SOUTH'
            },
        }
