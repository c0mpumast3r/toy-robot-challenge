
class InvalidCommandException(Exception):
    """Raised when an invalid command is given"""
    pass


class InvalidPositionException(Exception):
    """Raised when a position is specified that does not fit on the grid"""
    pass
