from util.base_exceptions import InvalidPositionException


class Position():
    """Class to represent Position or Location of an object"""

    def __init__(self):
        """Initialize all position attributes"""
        self._x = None
        self._y = None
        self._direction = None

    def set_position(self, grid, coordinates):
        """Sets object's position/coordinates with respect to grid

        Args:
            grid (Grid): contains Grid object information
            coordinates (dict): contains position/coordinates information

        Raises:
            InvalidPositionException: an exception thrown if coordinates is
                            invalid or out of bounds with respect to grid
        """
        if coordinates['x'] in grid.x_range and \
            coordinates['y'] in grid.y_range and \
                coordinates['direction'] in grid.directions:
            self._x = coordinates['x']
            self._y = coordinates['y']
            self._direction = coordinates['direction']
        else:
            raise InvalidPositionException

    def get_position(self):
        """Gets object's current position/coordinates

        Returns:
            dict: Returns position/coordinates
        """
        return dict(x=self._x, y=self._y, direction=self._direction)

    def __str__(self):
        return f"{self._x},{self._y},{self._direction}"
