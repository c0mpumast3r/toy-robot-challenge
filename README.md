# Toy Robot Challenge

## Description

A simple command line application described by [requirements.md](requirements.md)

## Preamble

This coding challenge is given to applicants who had succeeded the first stage of the recruitment process, albeit a
filtering tool. The hiring company uses these submissions to help understand the applicants abilities and provide a
discussion point should the applicant be successful in progressing to the next stage.

Apparently, a quick google search leads us to the original creator of this challenge, Jon Eaves in 2007
while recruiting for ANZ (See https://joneaves.wordpress.com/2014/07/21/toy-robot-coding-test/) and a couple
of attempts written in a variety of languages.

I had reviewed and tested some implementations and created a personal version that aims to improve and implement
best practices (using Single Responsibility and DRY design principles). This implementation is written in Python3.

## Key Points/Features

-   No obstructions on table surface/grid
-   Robot is free to roam but must be prevented from falling or going overboard the table/grid size (5x5)
-   Discards all commands until a valid PLACE command is executed
-   Handles error states (but as per spec, should ignore or discard invalid commands or positions)
-   Able to accept robust user inputs (spaces and capitalized/small letters)
-   Able to increase table/grid size
-   Terminal or Command line based application
-   Can be run in Linux/Windows

## Usage

To setup your virtual environment:

    make env

To activate your virtual environment:

    source env/bin/activate

To run tests:

    make test

To run the simulator interactively:

    ./simulate.py

To test commands while in interactive mode, type the following in the terminal or command line:
PLACE 0,0,NORTH
MOVE
RIGHT
MOVE
REPORT
