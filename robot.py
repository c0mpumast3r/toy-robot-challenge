from util.position import Position
from util.base_exceptions import InvalidCommandException, \
    InvalidPositionException

from re import compile, X

PATTERN = compile(
    r"""
        (?P<x>\d+),                  # x coord
        (?P<y>\d+),                  # y coord
        (?P<direction>NORTH|EAST|SOUTH|WEST) # direction
        """, X
)


class Robot():
    """Class Robot object"""

    def __init__(self):
        """Initializes robot position and grid awareness"""
        self._position = None
        self._grid = None

    def _has_position(method):
        """Python decorator to check if position exists"""

        def wrapper(self):
            if not self._position:
                return
            method(self)

        return wrapper

    def _rotate(self, direction):
        """Performs the actual rotation logic

        Args:
            direction (str): face direction of robot
                                (NORTH, EAST, WEST or SOUTH)
        """

        curr_position = self._position.get_position()
        try:
            curr_direction = curr_position['direction']
            curr_position['direction'] = self._grid.compass[curr_direction][direction]
            self._position.set_position(self._grid, curr_position)
        except InvalidPositionException:
            pass

    def parse(self, line: str):
        """Performs parsing of commands inputted in command line and return
        valid command components consisting of command and coordinates.

        Args:
            line (str): user inputted command (from stdin)

        Raises:
            InvalidCommandException: raised when command is invalid

        Returns:
            tuple: command and coordinates
        """

        line = line.upper()
        tokens = line.strip().split()

        cmd = tokens[0]
        if cmd not in {'MOVE', 'LEFT', 'RIGHT', 'REPORT', 'PLACE'}:
            raise InvalidCommandException

        coords = None
        if cmd == 'PLACE':
            if len(tokens) < 2:
                raise InvalidCommandException

            valid = PATTERN.search("".join(tokens[1:]))
            if not valid:
                raise InvalidCommandException

            coords = dict(
                x=int(valid['x']),  # regexp ensures the str is a digit
                y=int(valid['y']),
                direction=valid['direction'],
            )

        return cmd, coords

    def learn(self, **kwargs):
        """Enables robot to learn new stuff (such as grid information)"""
        if 'grid' in kwargs:
            self._grid = kwargs['grid']

    def place(self, position):
        """Place robot at the specified position on the grid

        Args:
            position (dict): new position of the robot
        """
        try:
            if not self._position:
                self._position = Position()

            self._position.set_position(self._grid, position)
        except InvalidPositionException:
            pass

    @_has_position
    def move(self):
        """Move the robot one unit in the direction it is currently facing"""

        curr_position = self._position.get_position()
        try:
            curr_direction = curr_position['direction']
            if curr_direction == 'NORTH':
                curr_position['y'] += 1
            elif curr_direction == 'EAST':
                curr_position['x'] += 1
            elif curr_direction == 'SOUTH':
                curr_position['y'] -= 1
            elif curr_direction == 'WEST':
                curr_position['x'] -= 1

            self._position.set_position(self._grid, curr_position)
        except InvalidPositionException:
            pass

    @_has_position
    def left(self):
        """Rotate the robot 90 degrees to the left"""
        self._rotate('left')

    @_has_position
    def right(self):
        """Rotate the robot 90 degrees to the right"""
        self._rotate('right')

    @_has_position
    def report(self):
        """Print the current position of the robot"""
        print(self._position)
