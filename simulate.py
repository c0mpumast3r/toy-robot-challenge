#! ./env/bin/python

import fileinput

from util.grid import Grid
from robot import Robot
from util.base_exceptions import InvalidCommandException


def main():
    """Toy Robot Challenge simulation"""

    grid = Grid(x_range=6, y_range=6)

    robot = Robot()
    robot.learn(grid=grid)

    for line in fileinput.input():
        try:

            (cmd, coords) = robot.parse(line)
            if cmd == 'PLACE':
                robot.place(coords)
            else:
                getattr(robot, cmd.lower())()

        except InvalidCommandException:
            # Silently ignore invalid commands
            pass


if __name__ == "__main__":
    main()
